#! /bin/bash
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`

LOG=OLD_LOGS
CONVO_LOG=convo_logs
if [ ! -d "$LOG" ]; then
    mkdir $LOG
fi

if [ ! -d "$CONVO_LOG" ]; then
    mkdir $CONVO_LOG
fi

pkill -f -9 python3
export LANG=C.UTF-8
#export SANIC_WORKERS=5
export ACTION_WORKERS=2
export BOT_ENV="dev"
cd main/
mv nohup.out ../$LOG/nohup.out-${DATE}.log
pkill -f -9 python3

nohup gunicorn rasa_action_server:app --bind 0.0.0.0:5002 --worker-class sanic.worker.GunicornWorker --log-level debug --workers=$ACTION_WORKERS &
 
