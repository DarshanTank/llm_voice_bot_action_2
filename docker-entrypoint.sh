#!/bin/bash
DATE=`/bin/date '+%Y-%m-%d-%H-%M-%S'`

LOG=OLD_LOGS
CONVO_LOG=logs
if [ ! -d "$LOG" ]; then
    mkdir $LOG
fi

if [ ! -d "$CONVO_LOG" ]; then
    mkdir $CONVO_LOG
fi

pkill -f -9 python3 
export LANG=C.UTF-8
export BOT_ENV="prod"
#export SANIC_WORKERS=4
cd main/

gunicorn rasa_action_server:app --bind 0.0.0.0:5002 --worker-class sanic.worker.GunicornWorker --log-level debug --workers=$ACTION_WORKERS