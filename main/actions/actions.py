# -*- coding: utf-8 -*-
# Author: Ekta
from cgitb import text
from code import interact
from logging import exception
import sys
from tabnanny import check
import re
sys.path.append('../')

from actions.get_config import get_replaced_message
from utils.time_valid import haptik_time_validation, validate_only_time
from utils.date_valid import validate_date, haptik_date_validation
from db.tracker_store_connection import load_config, MongoDB, RedisDB
from logger_conf.logger import get_logger, CustomAdapter
from raven import Client
from threading import Thread
from utils.faq import faq_body
from utils.text_format import color
from utils.email_alert import send_mail as alert
from utils.action_time import timeit
import dateutil.relativedelta as dr
from pytz import timezone
import datetime
import requests
import pickle
from constants import *
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction, UserUtteranceReverted, ActionReverted, Restarted, \
    EventType
from rasa_sdk import Action, Tracker
import json
from typing import Any, Text, Dict, List, Union, Optional
from rasa_sdk.forms import FormAction
from datetime import date, timedelta
import random
from calendar import c, monthrange
# from utils.Num2str.english import GenEnglishNum

from llm.convo_llm import get_llm_response

logger = get_logger(__name__)  # name of the module
logger = CustomAdapter(logger, {"sender_id": None})
# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])
# redis connection
redis_db_obj = RedisDB()
#Convert Num to Audio
# num_generator = GenEnglishNum()

# with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
#     templates_english = json.load(temp)


with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
    templates_english = json.load(temp)


# -------------------------------------------------------------------------------------------------------------------
# next 3 days dateupsell_existing_english

format = "%Y-%m-%d %H:%M:%S"
date_format = "%d/%m/%Y"
# Current time in UTC
now_utc = datetime.datetime.now(timezone('UTC'))
# print(now_utc.strftime(format))
# Convert to Asia/Kolkata time zone
now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
current_time_str = now_asia.strftime(format)
# todays_date = current_time_str.split(" ")[0]
# current_time= datetime.datetime.strptime(current_time_str, "%Y-%m-%d %H:%M:%S").time()
current_date = datetime.datetime.strptime(
    current_time_str, "%Y-%m-%d %H:%M:%S").date()
next_3rd_date = datetime.datetime.strptime(
    current_time_str, "%Y-%m-%d %H:%M:%S").date() + datetime.timedelta(days=3)
next_2nd_date = datetime.datetime.strptime(
    current_time_str, "%Y-%m-%d %H:%M:%S").date() + datetime.timedelta(days=2)
Next_3_Day_Date = next_3rd_date.strftime(date_format)
Next_2_Day_Date = next_2nd_date.strftime(date_format)
Today = current_date.strftime(date_format)
# -----------------------------------------------------------------------------------------------------------------------
# my_date = ""
# my_time = ""

# -----------------------------------------------------------------------------------------------------------------------
# colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple = color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
underline = color.UNDERLINE
end = color.END


# -----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------##
def get_template(language):
    lang_dict = {
        'english':templates_english,        
    }

    if language in lang_dict:
        return lang_dict[language]


def template_resolver(language, key):
    lang_dict = {
        'english':templates_english,        
    }

    if language in lang_dict:
        return lang_dict[language][key]


def key_check(language, key_to_check):
    lang_dict = {
        'english':templates_english,        
    }

    if language in lang_dict:
        if key_to_check in lang_dict[language]:
            return True
        else:
            return False

def get_stage_codes(stage_name, stage_code):
    stage_updates = {}
    stage_updates["stage"] = stage_name
    if stage_code != "FAQ":
        stage_updates["STAGE_CODE"] = stage_code

    return stage_updates



def get_stage(cust_info):
    stage_name = ""
    stage_name = cust_info["stage"]

    return stage_name



def isValid(text):
    # 1) Begins with 0 or 91
    # 2) Then contains 6,7 or 8 or 9.
    # 3) Then contains 9 digits
    Pattern = re.compile("(0|91)?[6-9][0-9]{9}")
    if Pattern.match(text):
        return 'Valid'
    else:
        return 'Invalid'

def get_bot_message(event,bot_msg, templates_temp):
    
    bot_msg = bot_msg.replace("fallback_word","")
    # logger.info(f"Template --       {templates_temp}")
    # logger.info("bot msg is :"+str(bot_msg))
    
    for template_key,template_message in templates_temp.items():
        # logger.info(f"bot msg is : {bot_msg}")
        # logger.info("Current template key is {} and current template_message is : {}".format(template_key,template_message))
        if bot_msg == template_message:
            logger.info("Template key is : "+str(template_key))
            if template_key.endswith("_short"):
                # logger.info("Key endswith short"+str(template_key))
                short_key = template_key.replace("_short","_short_2") 
            else:
                logger.info("Key does not endswith short"+str(template_key))
                short_key = template_key + "_short"
            
            if short_key in templates_temp:
                bot_msg = templates_temp[short_key]
                break
            elif template_key + "_short" in templates_temp:
                bot_msg = templates_temp[short_key]
                break
            else:
                if "who" in template_key or  "faq" in template_key:
                    continue
                
                else:
                    if template_key.endswith("_short") and template_key + "_short_2" in templates_temp:
                # logger.info("Key endswith short"+str(template_key))
                        short_key = template_key.replace("_short","_short_2") 
                        bot_msg = templates_temp[short_key]
                        break
                    else:
                        if template_key + "_short" in templates_temp:
                            logger.info("Key does not endswith short"+str(template_key))
                            short_key = template_key + "_short"
                            bot_msg = templates_temp[short_key]
                            break
                        else:
                            bot_msg = templates_temp[template_key]
                            break

    logger.info("Bot message returned is :"+str(bot_msg))
    return bot_msg



def get_slot_count(tracker,slot_name) -> int:
    """Count the number of times a slot has been requested.
    Arguments:
        tracker - the tracker
        slot_name - the slot to count
    Returns:
        count for given slot
    """
    logger.info(f"Inside get slot count")
    slot_count = 1
    prev_slot   = None
    try:
        for event in reversed(tracker.events):
            if event.get("event") == "slot":
                if (
                    event.get("name") == "requested_slot" and 
                    event.get("value") == slot_name
                ):
                    # if prev_slot == slot_name:
                    slot_count += 1
                    # else:
                    #     slot_count = 0
                    # prev_slot = slot_name
            
        logger.info(f"slot count is {slot_count}")
        logger.info(f"Exiting get slot count")
        return slot_count
    except Exception as e:
        logger.exception(f"Exception in get_slot_count : {e}")


########################################### PRE ACTIONS ###########################################

class ActionRepeat(Action):
    """
    Repeat the bot response again if user asked to repeat, also
    repeat the bot response if user doesn't say anything
    If asked continously asked to repeat for 2 times 3rd time
    end the call
    return: dispatch message for who called if silent after initial message
    return: dispatcher message for previous response
    """

    def name(self) -> Text:
        return "action_repeat"
    @timeit
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        sender_id = tracker.current_state()["sender_id"]
        stage_updates = {}
        try:
            logger.info("Executing action : {}".format(action_name), sender_id=sender_id)

            sender_id, cust_info = redis_db_obj.get_details(tracker)
            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info("inside repeat action: inside if redis is NONE")
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            # tracker details
            text = tracker.latest_message['text']
            intent = tracker.latest_message['intent'].get('name')
            confidence = tracker.latest_message['intent'].get('confidence')

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name) 
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            full_name = cust_info['full_name']
            language = cust_info['language'].lower()
            logger.info("language is :  "+str(language),sender_id=sender_id)
            
            bot_msg = ""
            
            stage = cust_info.get("stage")
            
            templates_temp = {}
            
            
            templates = get_template(language)
            full_name = str(cust_info.get('full_name'))
            language=cust_info.get('language')
        
            variable_replacement_map = {
            "<full_name>":full_name,
                }
            stage_updates = {}

            for key,message in templates.items():
                # replace the variable name in the templates with variable value extracted from db
                for var_name, actual_variable in variable_replacement_map.items():
                    message = message.replace(var_name,str(actual_variable))

                # replaced_date = reform_date(str(due_date))
                # message = message.replace("var_due_date", replaced_date)
                templates_temp[key] = message

            repeat = 0
            for event in reversed(tracker.events):
                logger.info(f"the events is #############{event}")
                if event.get("event") == "bot":
                    bot_not_answered = False
                    bot_msg = event.get("text")
                    logger.info(f"the tracker events is {bot_msg}")
                    logger.info("Fallback message not found")
                    bot_msg = event.get("text")
                    bot_msg = get_replaced_message(bot_msg, cust_info)
                    bot_msg = get_bot_message(event, bot_msg, templates_temp)
                    logger.info("Bot message after function call : "+str(bot_msg))
                    #if bot_msg is not None:
                    #repeat += 1
                    break
                

                else:
                    bot_not_answered = True

            for event in tracker.events:
                if event.get("event") == "bot":
                    # logger.info("action_repeat : sender id:" + str(sender_id) + " Bot has answered! ", sender_id=sender_id)
                    key = 'utter_initial_msg'
                    message = template_resolver(language, key)
                    message = get_replaced_message(message, cust_info)
                    logger.info(f"In Repeat Actio ------      {message}")
                    if event.get("text") == message.replace("<full_name>", full_name):
                        logger.info("setting true bot not answered ", sender_id=sender_id)
                        bot_not_answered = True
                    else:
                        logger.info("setting true bot not answered ", sender_id=sender_id)
                        bot_not_answered = False

                # logger.info("INSIDE REPEAT ACTION *** GET USER EVENT : "+ str(event.get("event"))) #+ "INTENT IS "+ str(event.get("parse_data")["intent"]["name"]))
                if event.get("event") == "user" and \
                        event.get("parse_data")["intent"]["name"] == "intent_repeat":
                    repeat += 1
                    logger.info("ActionRepeat: increment" + str(repeat), sender_id=sender_id)

                    ## update repeat count
                    final_dict = {
                        "repeat_count": repeat,
                        "action": "action_repeat"
                    }
                    is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

                elif event.get("event") == "user" and event.get("parse_data")["intent"]["name"] != "intent_repeat":
                    repeat = 0

            logger.info("increment after loop--> " + str(repeat), sender_id=sender_id)
            
            
            if repeat >=3:
                logger.info("ActionRepeat: Repeat Action", sender_id=sender_id)
                
                final_dict = {
                    "repeat_failure": "yes",
                    "action": "action_repeat"
                }
                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                key = 'utter_bot_not_understand'
                message = template_resolver(language, key)
                message = get_replaced_message(message, cust_info)
                dispatcher.utter_message(message)
                return []
                # Call is Ended EOC
            else:
                logger.info("BOT NOT ANSWERED VALUE IN ELSE PART OF REPEAT ACTION " + str(bot_not_answered),
                            sender_id=sender_id)
                logger.debug("ActionRepeat: Repeat Action", sender_id=sender_id)
                if (stage in ["","repeat:initial_message"]) and repeat==2:
                    stage = "repeat:initial_message"
                    
                    STAGE_CODE = "DSCN"

                    stage_updates = get_stage_codes(stage, STAGE_CODE)

                    final_dict = {
                        "call_comp_flag": "no",
                        "action": "action_repeat"
                    }
                    is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

                    logger.info("bot did not utter anything", sender_id=sender_id)
                    key = 'utter_bot_not_understand'
                    message = template_resolver(language, key)
                    message = get_replaced_message(message, cust_info)
                    dispatcher.utter_message(message)
                    return [UserUtteranceReverted()]
                elif bot_not_answered:
                    stage = "repeat:initial_message"
                    
                    STAGE_CODE = "DSCN"

                    stage_updates = get_stage_codes(stage, STAGE_CODE)

                    final_dict = {
                        "call_comp_flag": "no",
                        "action": "action_repeat"
                    }
                    is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

                    logger.info("bot did not utter anything", sender_id=sender_id)
                    
                    key = 'utter_initial_msg'
                    message = template_resolver(language, key)
                    message = get_replaced_message(message, cust_info)
                    dispatcher.utter_message(message)
                    return [UserUtteranceReverted()]
                logger.info("bot_not_answered is false", sender_id=sender_id)
                logger.info("Bot Message: " + str(bot_msg), sender_id=sender_id)
                dispatcher.utter_message(bot_msg)
                return [UserUtteranceReverted()]

        except Exception as e:
            logger.exception("$$--------------------" + red + " Exception in action repeat --> " + end + str(e),
                             sender_id=sender_id)
            client.captureException()
            return []

# Form Survey
class FormSurveyDetails(FormAction):

    """
    Capture Date of the payment
    """

    def name(self) -> Text:
        return "form_survey_details"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        return ["initial_affirm"]        


    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        return {
            "initial_affirm": self.from_text()
            }

    # noinspection PyUnusedLocal
    def request_next_slot(
            self,
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: Dict[Text, Any], ) -> Optional[List[EventType]]:
        """Request the next slot and utter template if needed,
            else return None"""
        sender_id, cust_info = redis_db_obj.get_details(tracker)
        action_name = "form_survey_details"

        if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
            logger.info("inside form_accept_offer: inside if redis is NONE")
            dispatcher.utter_message(text=" EOC")
            return [Restarted()]
        
        text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name) 
        sender_id, cust_info = redis_db_obj.get_details(tracker)

        language = cust_info['language'].lower()
        logger.info("language is :  "+str(language),sender_id=sender_id)

        try:
            for slot in self.required_slots(tracker):
                if self._should_request_slot(tracker, slot):
                    logger.info(f"Request next slot: @---+> '{slot}'", sender_id=sender_id)
                    
                    data = tracker.get_slot(slot)
                    logger.info(f"The slot value for {slot} is {data}", sender_id = sender_id)

                    # Ask for the payment date
                    if slot == "initial_affirm" and round(float(confidence), 2) > 0.65 :
                        stage = "survey_complete"
                        text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name) 
                        STAGE_CODE = "SURVEY_COMPLETE"
                        stage_updates = get_stage_codes(stage, STAGE_CODE)
                        final_dict = {
                        "call_comp_flag": "yes",
                        "action":action_name,
                        "right_person":True,
                        "free_text_first_stage":text

                        }
                        redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                        # update redis
                        count = get_slot_count(tracker,slot)
                        if count>1:
                            
                            if count >= 4:
                                key = "utter_bot_not_understand"

                            else:
                                key = "utter_thanks"

                            message = template_resolver(language, key)
                            message = get_replaced_message(message, cust_info)
                            dispatcher.utter_message(message , **tracker.slots)
                            return [SlotSet("requested_slot", slot)]
                        else:
                            key = "utter_thanks"
                            message = template_resolver(language, key)   
                            message = get_replaced_message(message, cust_info)
                            dispatcher.utter_message(message , **tracker.slots)
                            return [SlotSet("requested_slot", slot)]

                    

                    redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
                    logger.info(f"{green}slot is --------------> {slot}{end}")
                    logger.info(f"{green}{SlotSet('requested_slot', slot)}{end}")
                    return [SlotSet('requested_slot', slot)]
            return None
        except Exception as e:
            logger.info("EXCEPTION When request next slot: "+str(e))
            client.captureException()
            return [FollowupAction("action_fallback")]

    def validate_initial_affirm(self, value: Text,
           dispatcher: CollectingDispatcher,
           tracker: Tracker,
           domain: Dict[Text, Any]):
        sender_id,cust_info = redis_db_obj.get_details(tracker)
        # text = tracker.latest_message['text']
        # intent = tracker.latest_message['intent'].get('name')
        language = cust_info["language"].lower()
        action_name = "form_survey_details"
        text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name) 

        logger.info(f"Inside Slot Validation for slot inital affirm ---", sender_id = sender_id)
        final_dict={}

        stage = "SURVEY_COMPLETE"
        STAGE_CODE = "survey_complete"
        stage_updates = get_stage_codes(stage, STAGE_CODE) 
        final_dict.update({
                "call_comp_flag": "no",
                "action": action_name,
                "free_text_last_stage" : text
            })
        redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)
        
        return {"initial_affirm": text}

  
    def submit(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:

        try:
            sender_id = tracker.current_state()["sender_id"]
            action_name = "form_accept_offer"
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info("inside form_accept_offer: inside if redis is NONE")
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]

            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            language = cust_info['language'].lower()
            logger.info("language is :  "+str(language),sender_id=sender_id)
            stage = cust_info.get('stage')
            # if stage == 'Different payment mode':
            #     return [FollowupAction('form_different_payment_mode')]


            stage = "SURVEY_COMPLETE"
            STAGE_CODE = "survey_complete"
            stage_updates = get_stage_codes(stage, STAGE_CODE)
            final_dict = {
            "call_comp_flag": "yes",
            "action":action_name,
            }
            # update redis
            redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

            key="utter_thanks"
            message = template_resolver(language, key)
            message = get_replaced_message(message, cust_info)
            dispatcher.utter_message(message, **tracker.slots)

            return []

        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)))
            client.captureException()
            return []


class ActionCommonAction(Action):

    def name(self) -> Text:
        return "action_common_flow"

    @timeit
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        action_name = self.name()
        try:
            sender_id, cust_info = redis_db_obj.get_details(tracker)
            logger.info("Executing action : {}".format(action_name), sender_id=sender_id)
            redis_data = redis_db_obj.redis_get_dict_value(str(sender_id))
            logger.info("Executing action_redis_db_obj : {}".format(redis_data), sender_id=sender_id)

            if redis_db_obj.redis_get_dict_value(str(sender_id)) is None:
                logger.info("{} : customer data is not present".format(action_name), sender_id=sender_id)
                dispatcher.utter_message(text=" EOC")
                return [Restarted()]
            
            text, intent, confidence = redis_db_obj.tracker_details(tracker, action_name)
            sender_id, cust_info = redis_db_obj.get_details(tracker)

            user_name = str(cust_info["full_name"])
            Conversation_history =  redis_data["Conversation_history"]
            language = cust_info['language'].lower()

            logger.info("language is :  "+str(language),sender_id=sender_id)
            logger.info(str(language))

            if round(float(confidence), 2) > 0.10 :

                # if intent == "faq_other_modes_of_payment":
                #     return [FollowupAction("form_payment_mode")]

                # trigger_logic_dict = {
                #     "busy":{"key":"utter_thanks", "stage":"busy", "STAGE_CODE":"BUSY"},
                #    "dnd":{"key":"utter_thanks", "stage":"3rd_person", "STAGE_CODE":"DND"},
                #     "intent_deny":{"key":"utter_thanks", "stage":"busy", "STAGE_CODE":"BUSY"},
                #     "intent_affirm":{"key":"utter_thanks", "stage":"survey_complete", "STAGE_CODE":"SURVEY_COMPLETE"},
                #     "affirm_person_speaking":{"key":"utter_thanks", "stage":"survey_complete", "STAGE_CODE":"SURVEY_COMPLETE"},
                #     "right_time":{"key":"utter_thanks", "stage":"survey_complete", "STAGE_CODE":"SURVEY_COMPLETE"},
                #     "intent_repeat":{"key":"utter_thanks", "stage":"survey_complete", "STAGE_CODE":"SURVEY_COMPLETE"}
                #         }
                # trigger_logic_data = trigger_logic_dict.get(intent)

                # logger.info(f"The Intent data is {trigger_logic_data}")
                stage = "" #trigger_logic_data.get("stage")
                STAGE_CODE = "" #trigger_logic_data.get("STAGE_CODE")
                Conversation_history, message = get_llm_response(redis_data,Conversation_history,text)
                stage_updates = get_stage_codes(stage, STAGE_CODE)
                final_dict = {
                    "call_comp_flag": "yes",
                    "action":action_name,
                    "Conversation_history":Conversation_history,
                    "free_text_first_stage":text
                }                

                logger.info("{} : updating stage and other fields ".format(action_name), sender_id=sender_id)
                
                # update redis
                is_updated = redis_db_obj.update_stage(tracker, final_dict, stage_updates, action_name)

                # key = trigger_logic_data.get("key")
                # message = template_resolver(language, key)
                # message = message.replace("<full_name>", user_name)
                # Hit llm api and get response
                
                message = message.replace("%"," percentage").replace("!","").replace("&"," and")
                dispatcher.utter_message(message)
                logger.info("{} :Exiting action".format(action_name), sender_id=sender_id)
                return []

            else:
                # confidence is low hence route to fallback
                return [FollowupAction("action_fallback")]

        except Exception as e:
            logger.exception("Exception in {} : \n {} ".format(action_name, str(e)), sender_id=sender_id)
            client.captureException()
            return [Restarted()]

