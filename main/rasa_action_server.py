import sys

import redis
sys.path.append('../')
import os
import argparse
import logging
import types
from typing import List, Text, Union, Optional, Any
from ssl import SSLContext
import datetime
import pytz
from actions.get_config import get_replaced_message
from constants import *
from datetime import datetime, timezone
from datetime import date, timedelta
from sanic import Sanic, response
from sanic.response import HTTPResponse
from sanic.request import Request
from sanic_cors import CORS

from rasa_sdk import utils
from rasa_sdk.cli.arguments import add_endpoint_arguments
from rasa_sdk.constants import DEFAULT_SERVER_PORT
from rasa_sdk.executor import ActionExecutor
from rasa_sdk.interfaces import ActionExecutionRejection, ActionNotFoundException

import asyncio
import inspect
import json
import pytz
import pickle
import time
from datetime import datetime, timezone
from datetime import date, timedelta

from main.actions.actions import template_resolver, key_check 
from utils.email_alert import send_mail as alert
from utils.email_alert import send_converstaion_logs

from utils.text_format import color
from threading import Thread
from db.tracker_store_connection import load_config, MongoDB, RedisDB
from threading import Thread
from raven import Client

from logger_conf.logger import get_logger, CustomAdapter

logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

# time in IST  
utc_dt = datetime.now(timezone.utc)
IST = pytz.timezone('Asia/Kolkata')


# fetch the messages from the template file 
with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
    templates_english = json.load(temp)

# with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
#     templates_english = json.load(temp)




# loading endpoints
gen_config = load_config()
# redis connection
client = Client(gen_config["raven"]["url"])

mongo_conn = MongoDB()
redis_db_obj = RedisDB()

## colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple= color.PURPLE
yellow = color.YELLOW
bold = color.BOLD
underline = color.UNDERLINE
end = color.END


def alert_me(msg,sender_id, key="", wait=False):
    t = Thread(target=send_converstaion_logs, args=(msg,sender_id,key))
    t.start()
    if wait:
        t.join()

header_html = "<h3> Jarvis Karnataka Survey</h3>"
header_html_2 = "<h4> Sender id : s_id,<br> Mobile Number : m_no,<br> Stage Code : s_code, <br> Stage Name : s_name </h4>"

def send_conv_logs(redis_data,phone_number,sender_id,stage_code,stage_name,payment_date,another_mobile_number):
    # Email alert for the conversation logs
    conversation_logs = redis_data["conversation_log"]
    table_list = []
    for log in conversation_logs:
        single_conv = []
        for key, text in log.items():
            single_str = """<tr style=\"border: 1px solid black;\">
            <td style=\"border: 1px solid black;\">_key</td>
            <td style=\"border: 1px solid black;\">_value</td></tr>"""

            single_str = single_str.replace("_key",key).replace("_value",str(text))
            single_conv.append(single_str)
        table_list.append(single_conv)



    html_string = ""
    for h_tag in table_list:
        for sub_tag in h_tag:
            html_string = html_string + " " + sub_tag
    html_string = header_html \
        + header_html_2.replace("s_id", str(sender_id)).replace("m_no", str(phone_number)).replace("s_code", str(stage_code)).replace("s_name", str(stage_name)) \
            .replace("payment_date", str(payment_date)).replace("another_mobile_number", str(another_mobile_number))\
        + " " + "<table width=\"600\">"+html_string + "</table>"

    # uncomment only during Testing
    logger.info("Sending email alert")
    alert_me(html_string,sender_id)




app = Sanic(__name__, configure_logging=True)
executor = ActionExecutor()
action_package_name = "actions"
#executor = ActionExecutor()
logger.info("Executor registering the required action names")
executor.register_package(action_package_name)

def configure_cors(
    app: Sanic, cors_origins: Union[Text, List[Text], None] = ""
) -> None:
    """Configure CORS origins for the given app."""

    CORS(
        app, resources={r"/*": {"origins": cors_origins or ""}}, automatic_options=True
    )


def create_ssl_context(
    ssl_certificate: Optional[Text],
    ssl_keyfile: Optional[Text],
    ssl_password: Optional[Text] = None,
) -> Optional[SSLContext]:
    """Create a SSL context if a certificate is passed."""

    if ssl_certificate:
        import ssl

        ssl_context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
        ssl_context.load_cert_chain(
            ssl_certificate, keyfile=ssl_keyfile, password=ssl_password
        )
        return ssl_context
    else:
        return None


def create_argument_parser():
    """Parse all the command line arguments for the run script."""

    parser = argparse.ArgumentParser(description="starts the action endpoint")
    add_endpoint_arguments(parser)
    utils.add_logging_option_arguments(parser)
    return parser


def create_app(
    action_package_name: Union[Text, types.ModuleType],
    cors_origins: Union[Text, List[Text], None] = "*",
    auto_reload: bool = False,
) -> Sanic:
    """Create a Sanic application and return it.
    Args:
        action_package_name: Name of the package or module to load actions
            from.
        cors_origins: CORS origins to allow.
        auto_reload: When `True`, auto-reloading of actions is enabled.
    Returns:
        A new Sanic application ready to be run.
    """
    #app = Sanic(__name__, configure_logging=False)

    configure_cors(app, cors_origins)

    action_package_name = "actions"
    #executor = ActionExecutor()
    executor.register_package(action_package_name)

    return app


@app.get("/health")
async def health(_) -> HTTPResponse:
    """Ping endpoint to check if the server is running and well."""
    body = {"status": "ok"}
    return response.json(body, status=200)

@app.post("/webhook")
async def webhook(request: Request) -> HTTPResponse:
    """Webhook to retrieve action calls."""
    action_call = request.json
    if action_call is None:
        body = {"error": "Invalid body request"}
        return response.json(body, status=400)

    utils.check_version_compatibility(action_call.get("version"))
    auto_reload = False
    if auto_reload:
        executor.reload()

    try:
        result = await executor.run(action_call)
    except ActionExecutionRejection as e:
        logger.debug(e)
        body = {"error": e.message, "action_name": e.action_name}
        return response.json(body, status=400)
    except ActionNotFoundException as e:
        logger.error(e)
        body = {"error": e.message, "action_name": e.action_name}
        return response.json(body, status=404)

    return response.json(result, status=200)

@app.get("/actions")
async def actions(_) -> HTTPResponse:
    """List all registered actions."""
    auto_reload = False
    if auto_reload:
        executor.reload()

    body = [{"name": k} for k in executor.actions.keys()]
    return response.json(body, status=200)

@app.route("/initial_message", methods=["GET"])
async def initial_message(request: Request) -> HTTPResponse:
    try:
        logger.info('Argument Recieved are: '+str(request.args))
        logger.info('Mobile Number recieved is: '+str(request.args['mobile'][0]))
        # nach_user =  request.args['nach_user'][0]
        mobile = int(request.args['mobile'][0])
        condition = {"$or":[{"phone_number":mobile}]}
        q1 = condition # filter condition to find the document
        q2 = {"_id": 0}  # 2nd query condition optional, setting id column to 0 , or if only certain fields are req  OPTIONAL

        _id,sender_id='',''
        # Implement Object ID
        if '_id' in request.args and request.args['_id'][0]:
            logger.info('Object ID recieved is: '+str(request.args['_id'][0]))
            _id=str(request.args['_id'][0])
            sender_id=str(request.args['sender_id'][0])
            q1={'_id':_id}
            logger.info('*********Object Id*********  recieved is: '+str(q1))
        
        input_collection = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2)
        logger.info("Log Complete Record:"+str(input_collection))

        language = input_collection["language"].lower()

        if input_collection != None:

            language = language.lower()
            logger.info("LANGUAGE:"+str(language))
            logger.info("Phone number {} and Language is {}".format(str(mobile),str(language)))

            key = "utter_initial_msg"
            message = template_resolver(language.lower(), key)
            message = get_replaced_message(message, input_collection)
            # message = "Hello, I am Ria calling from Demo Credit Union. Am I speaking with Rohan"
            logger.info("Initial Message:"+str(message))            
            response_json = {
                    "text":message,
                    "keys":[]
                }

            Conversation_history = f""" 
"Assistant": "{message}" """
            
            update_record = {
                "customerCRTId":str(sender_id),
                "Conversation_history":Conversation_history,
                "free_text_last_stage": "",
            }

            is_updated = mongo_conn.mongo_update(col_type="input",record=update_record,upd_cond=q1)

            if is_updated:
                logger.info(f"******** Updated Sender id **************** {sender_id}")
            else:
                logger.info(f"******** Not Updated Sender id ****************{sender_id}")

        else:

            key = "utter_initial_msg"
            message = get_replaced_message(message, input_collection)
            message = template_resolver(language.lower(), key)
            logger.info("Initial Message:"+str(message))            
            response_json = {
                    "text":message,
                    "keys":[]
                }

    except Exception as e:
        logger.exception("Exception!! When inserting the new details to DB : " +str(e))
        client.captureException()
        key = "utter_initial_msg"
        message = template_resolver(language.lower(), key)

        response_json = {
                    "text":message,
                    "keys":[]
                }

    return response.json(response_json)

@app.route("/livecheck", methods=["GET"])
async def livecheck(request: Request) -> HTTPResponse:
    logger.info("==++=livecheck=++==")
    return response.json({"status": "ok"})

@app.route("/readycheck", methods=["GET"])
async def readycheck(request: Request) -> HTTPResponse:
    logger.info("==++=readycheck=++==")
    return response.json({"status": "ok"})

def call_data_thread(request_json):
    # logger.info("=============Entered call status function============")
    stop_calling = False
    try:

        call_status_main_dict = request_json
        if "call_status" in call_status_main_dict:
            call_status_dict = call_status_main_dict["call_status"]        
            
            if "callStatus" in call_status_dict and "customerPhone" in call_status_dict and "callEndTime" in call_status_dict:
                phone_number = int(call_status_dict["customerPhone"])
                sender_id = str(call_status_dict["customerCRTId"])
                # logger.debug("Updating the call status for : "+str(phone_number))
                logger.info("Updating the call status for : "+str(sender_id))
                
                current_date_time = datetime.now(IST)
                todays_date_time = current_date_time.strftime("%d/%m/%Y %H:%M:%S")
                todays_date = current_date_time.strftime("%d/%m/%Y")

                # last triggered date
                last_triggered_date = datetime.strptime(todays_date_time,'%d/%m/%Y %H:%M:%S') - timedelta(hours=5, minutes=30)
                last_triggered_date =  "ISODate"+"("+str(last_triggered_date)+")"

                cTime = datetime.now(IST)
                callTime = "ISODate"+"("+cTime.isoformat()+")"

                logger.info("call status dict:  "+str(call_status_dict))


                # dstPhone
                try:
                    if call_status_dict["dstPhone"] != None:
                        dstPhone = call_status_dict["dstPhone"]

                    else:
                        dstPhone = ''
                except Exception as e:
                    logger.error("Error in dstPhone taking: Exception: "+ str(e)) 
                    client.captureException()
                    dstPhone = ''  

                # flow_id
                try:
                    if call_status_dict["flow_id"] != None:
                        flow_id = call_status_dict["flow_id"]

                    else:
                        flow_id = ''

                except Exception as e:
                    logger.error("Error in flow_id taking: Exception :"+ str(e)) 
                    client.captureException()
                    flow_id = ''  
                
                try:
                    if call_status_dict["callEndTime"] != None and call_status_dict["callEndTime"] != '':
                        callEndTime = call_status_dict["callEndTime"][:19]
                        callEndTime = datetime.strptime(callEndTime,'%Y/%m/%d %H:%M:%S') - timedelta(hours=5, minutes=30)
                        callEndTime =  "ISODate"+"("+callEndTime.isoformat()+")"

                        # Have to check the in-consistency in the date splitting
                        date_split = call_status_dict["callEndTime"].split(" ")[0].split("/")
                        new_date = date_split[2]+"/"+date_split[1]+"/"+date_split[0]

                    else:
                        callEndTime = ''

                except Exception as e:
                    logger.error("Error in callEndTime taking: Exception : "+ str(e))
                    client.captureException() 
                    callEndTime = '' 
                    new_date = ''      
                
                try:
                    if call_status_dict["callConnectedTime"] != None and call_status_dict["callConnectedTime"] != '':
                        callConnectedTime = call_status_dict["callConnectedTime"][:19]
                        callConnectedTime = datetime.strptime(callConnectedTime,'%Y/%m/%d %H:%M:%S') - timedelta(hours=5, minutes=30)
                        callConnectedTime =  "ISODate"+"("+callConnectedTime.isoformat()+")"
                    else:
                        callConnectedTime = ''
                except Exception as e:
                    logger.error("Error in callConnectedTime taking: Exception: "+ str(e))
                    client.captureException()
                    callConnectedTime = ''
                
                
                upload_call_info = {'call_status':call_status_dict["callStatus"],'callConnectedTime' : callConnectedTime,\
                    'callEndTime' : callEndTime,'customerCRTId' : str(call_status_dict["customerCRTId"]),\
                    'srcPhone': call_status_dict["srcPhone"],'ringingTime': int(call_status_dict["ringingTime"]), \
                    'setupTime': int(call_status_dict["setupTime"]),'callType':call_status_dict['callType'],\
                    'tempDate':new_date, 'callTime':callTime,'flow_id':flow_id,'dstPhone':dstPhone,
                    }
                trigger_call = True
                try:
                    # taking data from redis if present
                    redis_data = redis_db_obj.redis_get_dict_value(str(sender_id))
                    call_status = call_status_dict["callStatus"]
                    

                    if redis_data is None:
                        # There is no data present in redis. Since the GRPC has not hit the bot's custom connector not even once
                        # This case will handle NO ANSWER and BUSY states and also ANSWERED if call get disconnected at the welcome message
                        try:
                            logger.info("Updating Sequence info! Extracting sequence number and no answer flag")
                            environment = os.environ["BOT_ENV"]
                            
                            q1 = {"customerCRTId":str(sender_id)}
                            q2 = {"_id":0} #Column filter condition
                            call_trigger_count = mongo_conn.mongo_get_collection_data(col_type="input",q1=q1,q2=q2) 
                            logger.info("Extracted value of call trigger count inside redis data None condition --> "+ str(call_trigger_count))
                            
            
                            if call_trigger_count is not None:
                                #moved flow type inside.
                                logger.info("Incrementing the sequence number and no answer seq !! ")
                                logger.info(call_trigger_count)
                            
                                if call_status == "ANSWERED":
                                    sts = "PARTIAL ANSWER"
                                    
                                        
                                    stage = "initial_message"
                                    stage = "initial_message"
                                    STAGE_CODE = "DSCN"
                                    call_trigger_count["STAGE_CODE"] = STAGE_CODE
                                    
                                   
                                    
                                    call_trigger_count['main_ans_seq'] += 1
                                    call_trigger_count['main_seq_no'] += 1
                                    logger.info("Partial Answer answered seq count: -- > "+ str(call_trigger_count['main_ans_seq']) )

                                # For "NO ANSWER" , "FAILED" and "BUSY"    
                                else:
                                    logger.info("inside else for no answer seq count:--> "+ str(call_trigger_count['main_no_ans_seq']))
                                    
                                        
                                    stage = ""
                                    stage = ""
                                    
                                    STAGE_CODE = "RNR"
                                    call_trigger_count["STAGE_CODE"] = STAGE_CODE
                                    
                                    call_trigger_count['main_no_ans_seq'] += 1
                                    call_trigger_count['main_seq_no'] += 1
                                    
                                    sts = call_status
                                                                       

                                # since redis data is none , hence stage will be empty , call_comp_flag will be set to no
                                # update parameters for customer details
                                update_seq = {
                                    "right_person" : call_trigger_count.get('right_person',False),
                                    "free_text_first_stage" : call_trigger_count.get('free_text_first_stage',""),
                                    "free_text_last_stage": call_trigger_count.get('free_text_last_stage',""),
                                    'main_seq_no': int(call_trigger_count['main_seq_no']),
                                    'main_no_ans_seq': int(call_trigger_count['main_no_ans_seq']),
                                    'main_ans_seq': int(call_trigger_count['main_ans_seq']),
                                    "last_triggered_date":last_triggered_date,
                                    "call_status":call_status,
                                    "stage":stage,
                                    "stage":stage,
                                    "STAGE_CODE": STAGE_CODE,
                                    "sts":sts,
                                    "flow_id":flow_id,
                                    "call_comp_flag": "no",
                                    "conversation_log": [],
                                    "setupTime": int(call_status_dict["setupTime"]),
                                    "customerCRTId": str(sender_id),
                                    }

                                # Give the max_trigger based on the trigger_times
                                if "trigger_times" in call_trigger_count:
                                    trigger_times = int(call_trigger_count["trigger_times"])
                                else:
                                    trigger_times = 3


                                # Check for call sequence mapping
                                if "call_sequence_mapping" in call_trigger_count:
                                    
                                    
                                    # check if todays date is present in the call sequence mapping
                                    # if present then call has been already triggered today
                                    
                                    if str(todays_date) in call_trigger_count["call_sequence_mapping"]:
                                        logger.info("Call has been triggered today :"+str(call_trigger_count["call_sequence_mapping"]))
                                        call_trigger_count["call_sequence_mapping"][str(todays_date)]["calls_triggered"] +=1
                                        #call_trigger_count["call_sequence_mapping"][str(todays_date)]["answered"] +=1
                                        if call_status == "ANSWERED":
                                            call_trigger_count["call_sequence_mapping"][str(todays_date)]["answered"] +=1
                                        else:
                                            call_trigger_count["call_sequence_mapping"][str(todays_date)]["not_answered"] +=1
                                    else:
                                        #Call has not been triggered for todays date hence update initial fields dict
                                        call_trigger_count["dscn_count"] = 0
                                        if call_status == "ANSWERED":
                                            call_trigger_count["call_sequence_mapping"][str(todays_date)] = {
                                                "calls_triggered":1,
                                                "answered":1,
                                                "not_answered":0, 
                                                "max_trigger":trigger_times
                                            }
                                        else:
                                            call_trigger_count["call_sequence_mapping"][str(todays_date)] = {
                                                "calls_triggered":1,
                                                "answered":0,
                                                "not_answered":1, 
                                                "max_trigger":trigger_times
                                            }
                                else:
                                    if call_status == "ANSWERED":
                                        call_trigger_count["call_sequence_mapping"] = {}
                                        call_trigger_count["call_sequence_mapping"][str(todays_date)] = {
                                            "calls_triggered":1,
                                            "answered":1,
                                            "not_answered":0, 
                                            "max_trigger":trigger_times
                                        }
                                    else:
                                        call_trigger_count["call_sequence_mapping"] = {}
                                        call_trigger_count["call_sequence_mapping"][str(todays_date)] = {
                                            "calls_triggered":1,
                                            "answered":0,
                                            "not_answered":1, 
                                            "max_trigger":trigger_times
                                        }

                                
                                call_answered_today = call_trigger_count["call_sequence_mapping"][str(todays_date)]["answered"]

                                # # , "AP", "VAL_DATE", "PAYMENT_DATE",

                                if call_trigger_count["call_sequence_mapping"][str(todays_date)]["calls_triggered"] \
                                    >= call_trigger_count["call_sequence_mapping"][str(todays_date)]["max_trigger"]:
                                    stop_calling = True
                                elif(call_trigger_count["call_sequence_mapping"][str(todays_date)]["answered"] >= call_trigger_count["call_sequence_mapping"][str(todays_date)]["max_trigger"]):
                                    stop_calling = True
                                elif (call_trigger_count["call_sequence_mapping"][str(todays_date)]["not_answered"]>=call_trigger_count["call_sequence_mapping"][str(todays_date)]["max_trigger"] ) :
                                    stop_calling = True
                                else:
                                    stop_calling = False



                                


                                logger.info("*"*50)
                                logger.info("Call Sequence mapping : "+str(call_trigger_count["call_sequence_mapping"]))
                                logger.info("call status : "+str(call_status))
                                logger.info("Main flow disposition is : "+str(STAGE_CODE))
                                logger.info("*"*50)

                                if stop_calling:
                                    date_time_delta = datetime.now(IST)+timedelta(days=1)
                                    call_trigger_count["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                else:
                                    call_trigger_count["next_trigger_date"] = current_date_time
                                    temp_val_date = call_trigger_count["next_trigger_date"]
                                    call_trigger_count["next_trigger_date"] = "ISODate"+"("+str(temp_val_date)+")"
                                


                                current_time = datetime.now(IST) 
                                current_hour = current_time.hour
                                current_date_time = datetime.now(IST)

                                logger.info(f" Date time with IST {current_date_time}  and without IST {current_time}", sender_id = sender_id)

                                
                                trigger_dict = {
                                        
                                        "WRNG":{
                                        "max_trigger" :1,
                                        "trigger_call":False,
                                        "trigger_times":1,
                                        "days" : 1,
                                        "hour":9,
                                        "hours":1,
                                        "pre_max_call":1,
                                        "post_max_call":2,
                                        "min_current_hour":8,
                                        "max_current_hour":18,
                                        },
                                        "SRVCOMPLT":{
                                        "max_trigger" :1,
                                        "trigger_call":False,
                                        "trigger_times":1,
                                        "days" : 1,
                                        "hour":9,
                                        "hours":1,
                                        "pre_max_call":1,
                                        "post_max_call":2,
                                        "min_current_hour":8,
                                        "max_current_hour":18,
                                        }
                                        
                                }
                                
                                if call_trigger_count.get("STAGE_CODE") in trigger_dict:
                                    STAGE_CODE = call_trigger_count.get("STAGE_CODE")
                                    data = trigger_dict.get(STAGE_CODE)
                                    logger.info(f"IN CAll Status Function with STAGE CODE {STAGE_CODE}------------------------------------", sender_id = sender_id)

                                    logger.info(f"For {STAGE_CODE} -----       and data is  {data}", sender_id=sender_id)
                                    stop_calling = True
                                    call_trigger_count["max_trigger"] = data["max_trigger"]
                                    call_trigger_count["trigger_times"] = data["trigger_times"]
                                    call_trigger_count["trigger_call"] = data["trigger_call"]
                                    date_time_delta = current_date_time.replace(hour= 9) + timedelta(days = data["days"])
                                    call_trigger_count["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                    
                                else:
                                    STAGE_CODE = call_trigger_count.get("STAGE_CODE")
                                    logger.info("IN CAll Status Function ------------------------------------", sender_id = sender_id)

                                    if call_answered_today >= 3:
                                        logger.info(f"For {STAGE_CODE} -----       ", sender_id=sender_id)
                                        stop_calling = True
                                        call_trigger_count["max_trigger"] = 3
                                        call_trigger_count["trigger_call"] = True
                                        date_time_delta = current_date_time.replace(hour= 9) + timedelta(days = 1)
                                        call_trigger_count["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                    else:
                                        if current_hour > 9 and current_hour <= 18:
                                            logger.info(f"Inside STAGE CODE {STAGE_CODE} if time b/w 9 and 8", sender_id = sender_id)
                                            date_time_delta = current_date_time + timedelta(hours=2)
                                            call_trigger_count["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                        else :
                                            logger.info(f"Inside STAGE CODE {STAGE_CODE} if time not b/w 9 and 8", sender_id = sender_id)
                                            date_time_delta = current_date_time.replace(hour= 9) + timedelta(days = 1)
                                            call_trigger_count["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"


                                    

                                call_trigger_count["call_sequence_mapping"][str(todays_date)]["max_trigger"] = 3#call_trigger_count["max_trigger"]
                                logger.info("Next trigger date is :"+str(call_trigger_count["next_trigger_date"]), sender_id = sender_id)
                                

                                updated_dict = {**call_trigger_count, **update_seq}
                                logger.info(f"######################################################{updated_dict}")

                                # logger.info("Updating input collecton with : " + str(updated_dict))
                                environment = os.environ["BOT_ENV"]
                                
                                upd_cond = {"customerCRTId":str(sender_id)}

                                if upload_call_info['ringingTime']==0:
                                    complete_dict = {**updated_dict, **upload_call_info}
                                    logger.info("inside reingtime #############################################")
                                    is_updated = mongo_conn.mongo_insert(col_type="reject",record=complete_dict)
                                    if is_updated is True:
                                        logger.info(f"Updated the reject collection with the update sequence for sender ID {sender_id}")
                                    else:
                                        logger.info(f" Failed to update in the reject collection for sender ID {sender_id}")
                                    status_update = {"call_status":"Call Status Updation Failure"}
                                    del_track = redis_db_obj.redis_delete_tracker(str(sender_id))
                                    return response.json(status_update)


                                is_updated = mongo_conn.mongo_update(col_type="input",record=updated_dict,upd_cond=upd_cond)

                                if is_updated is True:
                                    logger.info("Updated the input collection with the update sequence")
                                else:
                                    logger.info(" Failed to update in the input collection")

                                # Merge data from input collection and call status
                                complete_dict = {**updated_dict, **upload_call_info}
                                # Inserted updated record to mongo output collection
                                is_inserted = mongo_conn.mongo_insert(col_type="output",record=complete_dict)

                                if is_inserted is True:
                                    logger.info("INSERTED IN REPORT DB ")
                                else:
                                    logger.info("Failed to insert in Report DB. Please see logs for errors ")
                                
                                logger.info("=============Exit call status function============")
                                logger.info(cyan+"<-----------------------------------------------------callstaus----------------------------------->\n\n"+end)

                            else:
                                logger.info(f"Error!! When REDIS is None and not able to fetch record for phone number:==> {phone_number}")
                                logger.info(f"Phone number:==> {phone_number} || Call Data: ==> {upload_call_info}")
                                
                        except Exception as e:
                            logger.exception("$$----------------------------------------"+red+"EXCEPTION IN WHEN REDIS DATA IS NONE:--> "+end+ str(e))
                            client.captureException()

                    #This case will handle 'ANSWERED':
                    else:
                        
                        try:
                            # sender_id
                            logger.debug(f'This is redis data {redis_data}')
                            sender_id = str(call_status_dict["customerCRTId"])
                            
                            if str(todays_date) not in redis_data["call_sequence_mapping"]:
                                redis_data["dscn_count"] = 0

                            # Data is present in redis with atleast one hit to the bot
                            logger.info("Updating Sequence info! Redis Data is present.")
                            
                            logger.info("CustomerCRTID  is : "+str(sender_id))
                            
                            conversation_logs  = redis_data["conversation_log"]
                            

                            # this will always increase
                            
                            # Overall call trigger count - Main
                            trigger_count_seq = int(redis_data['main_seq_no']) + 1
                            # ANSWERED call trigger count - Main
                            trigger_count_answered_seq = int(redis_data['main_ans_seq']) + 1
                           


                            # Give the max_trigger based on the trigger_times
                            if "trigger_times" in redis_data:
                                trigger_times = int(redis_data["trigger_times"])
                            else:
                                trigger_times = 3


                            # Check for call sequence mapping
                            if "call_sequence_mapping" in redis_data:
                                
                                # check if todays date is present in the call sequence mapping
                                # if present then call has been already triggered today
                                
                                if str(todays_date) in redis_data["call_sequence_mapping"]:
                                    logger.info("Call has been triggered today :"+str(redis_data["call_sequence_mapping"]))
                                    redis_data["call_sequence_mapping"][str(todays_date)]["calls_triggered"] +=1
                                    redis_data["call_sequence_mapping"][str(todays_date)]["answered"] +=1
                                else:              
                                    redis_data["dscn_count"] = 0                              
                                    #Call has not been triggered for todays date hence update initial fields dict
                                    redis_data["call_sequence_mapping"][str(todays_date)] = {
                                        "calls_triggered":1, 
                                        "answered":1,
                                        "not_answered":0,
                                        "max_trigger":trigger_times
                                    }
                               
                            else:
                                redis_data["call_sequence_mapping"] = {}
                                redis_data["call_sequence_mapping"][str(todays_date)] = {
                                    "calls_triggered":1, 
                                    "answered":1,
                                    "not_answered":0,
                                    "max_trigger":trigger_times
                                }
                                
                            
                            if redis_data["call_sequence_mapping"][str(todays_date)]["calls_triggered"] \
                                >= redis_data["call_sequence_mapping"][str(todays_date)]["max_trigger"]:
                                stop_calling = True
                            elif redis_data["call_sequence_mapping"][str(todays_date)]["answered"] >= redis_data["call_sequence_mapping"][str(todays_date)]["max_trigger"]:
                                stop_calling = True
                            elif redis_data["call_sequence_mapping"][str(todays_date)]["not_answered"]>= redis_data["call_sequence_mapping"][str(todays_date)]["max_trigger"]:
                                stop_calling = True
                            else:
                                stop_calling = False

                             

                            
                            current_time = datetime.now(IST) 
                            current_hour = current_time.hour
                            current_date_time = datetime.now(IST)

                            trigger_dict = {
                                        
                                        "WRNG":{
                                        "max_trigger" :1,
                                        "trigger_call":False,
                                        "trigger_times":1,
                                        "days" : 1,
                                        "hour":9,
                                        "hours":1,
                                        "pre_max_call":1,
                                        "post_max_call":2,
                                        "min_current_hour":8,
                                        "max_current_hour":18,
                                        },
                                        "SRVCOMPLT":{
                                        "max_trigger" :1,
                                        "trigger_call":False,
                                        "trigger_times":1,
                                        "days" : 1,
                                        "hour":9,
                                        "hours":1,
                                        "pre_max_call":1,
                                        "post_max_call":2,
                                        "min_current_hour":8,
                                        "max_current_hour":18,
                                        }
                                        
                                }
                                
                            
                            call_answered_today = redis_data["call_sequence_mapping"][str(todays_date)]["answered"]
                                
                            
                            STAGE_CODE = redis_db_obj.redis_key_confirm("STAGE_CODE",redis_data)
                            
                            stage = redis_db_obj.redis_key_confirm("stage",redis_data)
                            stage = redis_db_obj.redis_key_confirm("stage",redis_data)
                            

                                                    
                            ## confirm redis keys 
                            STAGE_CODE = redis_db_obj.redis_key_confirm("STAGE_CODE",redis_data)
                            stage = redis_db_obj.redis_key_confirm("stage",redis_data)
                            stage = redis_db_obj.redis_key_confirm("stage",redis_data)
                            call_comp_flag = redis_db_obj.redis_key_confirm("call_comp_flag",redis_data)

                            # By default date provided is NONE
                            date_provided = None

                            
                            if  STAGE_CODE == "" or STAGE_CODE == "RNR" and len(conversation_logs) > 0:
                                STAGE_CODE = "DSCN" 
                                stage = "initial_message"
                                stage = "initial_message"
                            
                            
                            #update parameters for input table
                            update_seq = {
                                "right_person" : redis_data.get('right_person',False),
                                "free_text_first_stage" : redis_data.get('free_text_first_stage',""),
                                "free_text_last_stage": redis_data.get('free_text_last_stage',""),
                                'main_seq_no':trigger_count_seq,
                                "main_ans_seq":trigger_count_answered_seq,
                                "call_comp_flag":call_comp_flag,
                                "last_triggered_date":last_triggered_date,
                                "call_status":call_status,
                                "sts":call_status,
                                "stage":stage,
                                "stage":stage,
                                "STAGE_CODE":STAGE_CODE,
                                "flow_id":flow_id,
                                "customerCRTId":sender_id,
                                "conversation_log": conversation_logs,
                                "setupTime": int(call_status_dict["setupTime"]),
                                "call_sequence_mapping": redis_data["call_sequence_mapping"],

                                }
                            
                            form_fields = {}
                            for req_field in FIELD_TO_FILL:
                                form_slot_value = redis_data.get(req_field, None) or call_status_dict.get(req_field)
                                form_fields[req_field] = form_slot_value
                            update_seq = {**update_seq, **form_fields}
                
                            customer_details_update = update_seq

                            if redis_data.get("STAGE_CODE") in trigger_dict:
                                STAGE_CODE = redis_data.get("STAGE_CODE")
                                data = trigger_dict.get(STAGE_CODE)
                                logger.info(f"IN CAll Status Function with STAGE CODE {STAGE_CODE}------------------------------------", sender_id = sender_id)
                                logger.info(f"For {STAGE_CODE} -----       and data is  {data}", sender_id=sender_id)
                                stop_calling = True
                                redis_data["max_trigger"] = data["max_trigger"]
                                redis_data["trigger_times"] = data["trigger_times"]
                                redis_data["trigger_call"] = data["trigger_call"]
                                
                                date_time_delta = current_date_time.replace(hour= 9) + timedelta(days = data["days"])
                                redis_data["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                customer_details_update["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                customer_details_update["max_trigger"] = data["max_trigger"]
                                customer_details_update["trigger_times"] = data["trigger_times"]
                                customer_details_update["trigger_call"] = data["trigger_call"]

                            else:
                                STAGE_CODE = redis_data.get("STAGE_CODE")
                                logger.info("IN CAll Status Function ------------------------------------", sender_id = sender_id)
                                if call_answered_today >= 3:
                                    logger.info(f"For {STAGE_CODE} -----       ", sender_id=sender_id)
                                    stop_calling = True
                                    redis_data["max_trigger"] = 3
                                    redis_data["trigger_call"] = True
                                    date_time_delta = current_date_time.replace(hour= 9) + timedelta(days = 1)
                                    redis_data["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                    customer_details_update["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                    customer_details_update["max_trigger"] = 3
                                    customer_details_update["trigger_call"] = True
                                else:
                                    if current_hour > 9 and current_hour <= 18:
                                        logger.info(f"Inside STAGE CODE {STAGE_CODE} if time b/w 9 and 8", sender_id = sender_id)
                                        date_time_delta = current_date_time + timedelta(hours=2)
                                        redis_data["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                        customer_details_update["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                       
                                    else :
                                        logger.info(f"Inside STAGE CODE {STAGE_CODE} if time not b/w 9 and 8", sender_id = sender_id)
                                        date_time_delta = current_date_time.replace(hour= 9) + timedelta(days = 1)
                                        redis_data["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                        customer_details_update["next_trigger_date"] = "ISODate"+"("+date_time_delta.isoformat()+")"
                                
                            
                              
                            redis_data["call_sequence_mapping"][str(todays_date)]["max_trigger"] = 3

                            
                            logger.info("Next trigger date is :"+str(redis_data["next_trigger_date"]))
                        
                            logger.info("\nUpdating input collecton with : "+str(customer_details_update)+"\n")

                            upd_cond = {"customerCRTId":str(sender_id)}
                            if '_id' in redis_data and redis_data['_id']:
                                upd_cond = {'_id':str(redis_data['_id'])}
                                logger.info("********OBJECT ID FETCHED*************")
                            
                            is_updated = mongo_conn.mongo_update(col_type="input",record=customer_details_update,upd_cond=upd_cond)
                            
                            if is_updated is True:
                                logger.info("Custom Connector : Updated the customer details table for ANSWERED")      
                            else:
                                logger.info("Custom Connector: Failed to update the input collection") 
                            
                            # function call to send conversation logs
                            if os.environ["BOT_ENV"]!="prod":
                               
                                payment_date = redis_data.get("date_of_payment")
                                
                                s_name = redis_data.get("stage")
                                
                                # s_name = redis_data.get("stage")
                                s_code = redis_data.get("STAGE_CODE")
                                another_mobile_number= redis_data.get("another_mobile_number")
                                send_conv_logs(customer_details_update,phone_number, sender_id,s_code,s_name,payment_date,another_mobile_number)

                            # Inserting into report table
                            updated_dict = {**customer_details_update, **redis_data, **upload_call_info, **update_seq}
                            
                            # logger.info("Complete dict for inserting in output db:==> "+str(updated_dict)+"\n")
                            is_inserted = mongo_conn.mongo_insert(col_type="output",record=updated_dict)
                            if is_inserted is True:
                                logger.info("Inserted into Report table with updated sequences")
                            else:
                                logger.info("Failed inserting in Report table")

                            
                            # delete record for that sender ID
                            logger.info("DELETING phone number from REDIS")
                            del_val = redis_db_obj.redis_delete_value(str(sender_id))
                            logger.info("del_val of redis delete value --> "+ str(del_val))

                            # delete tracker for sender ID
                            logger.info("DELETING tracker from REDIS")
                            del_track = redis_db_obj.redis_delete_tracker(str(sender_id))
                            logger.info("del_track of redis delete value --> "+ str(del_track))

                            # logger.info("phone number expired from redis")
                            logger.info("=============Exit call status function============")
                        
                        except Exception as e:
                            try:
                                t1 = Thread(target=alert,args=("Exception!! when redis data is present"+str(e),))
                                t1.start()
                            except Exception as e:
                                logger.exception("Exception in sending alert--> when redis data is present"+str(e))
                            client.captureException()
                            logger.exception(red+"EXCEPTIOIN IN WHEN REDIS DATA PRESENT --> "+ str(e)+end)

                except Exception as e:
                    try:
                        t1 = Thread(target=alert,args=("Exception!! when inserting data to mongo",))
                        t1.start()
                    except Exception as e:
                        logger.error("Exception in sending alert-->  when inserting data to mongo"+str(e))
                    client.captureException()
                    logger.error("Failed to insert data to mongo: Error:- " + str(e))

                status_update = {"call_status":"Updated Call Status Successfully"}
                return response.json(status_update)
            else:
                logger.error("Few keys missing in the call status dictionary!")
                status_update = {"call_status":"Call Status Updation Failure"}
                return response.json(status_update)
        else:
            logger.error("Call status dictionary not recieved!")
            status_update = {"call_status":"Call Status Updation Failure"}
            return response.json(status_update)
    except:
        try:
            t1 = Thread(target=alert,args=("Exception!! When updating the call Status",))
            t1.start()
        except Exception as e:
            logger.error("Exception in sending alert-->  When updating the call Status"+str(e))
        client.captureException()    
        logger.error("Exception!! When updating the call Status")
        status_update = {"call_status":"Call Status Updation Failure"}
        return response.json(status_update)


@app.post("/call_status")
async def call_data(request):
    logger.info("=============Entered call status function============")
    try:
        request_json_object = request.json
        t1 = Thread(target=call_data_thread, args=(request_json_object,))
        t1.start()
    except Exception as e:
        logger.error("Exception in creating call status thread" + str(e))
    logger.info("=============Exiting call status function============")
    status_update = {"call_status": "Updated Call Status Successfully"}
    return response.json(status_update)

def run(
    action_package_name: Union[Text, types.ModuleType],
    port: Union[Text, int] = DEFAULT_SERVER_PORT,
    cors_origins: Union[Text, List[Text], None] = "*",
    ssl_certificate: Optional[Text] = None,
    ssl_keyfile: Optional[Text] = None,
    ssl_password: Optional[Text] = None,
    auto_reload: bool = False,
) -> None:
    logger.info("Starting action endpoint server...")
    if action_package_name is None:
        action_package_name = "actions"
    app = create_app(
        action_package_name, cors_origins=cors_origins, auto_reload=auto_reload
    )
    ssl_context = create_ssl_context(ssl_certificate, ssl_keyfile, ssl_password)
    protocol = "https" if ssl_context else "http"

    logger.info(f"Action endpoint is up and running on {protocol}://localhost:{port}")
    app.run("0.0.0.0", port, ssl=ssl_context, workers=1)


if __name__ == "__main__":
    # import rasa_sdk.__main__

    # rasa_sdk.__main__.main()

    # logging.basicConfig(level=logging.DEBUG)
    # logging.getLogger("matplotlib").setLevel(logging.WARN)
    
    logger.info("Starting Action Server!")
    arg_parser = create_argument_parser()
    args = arg_parser.parse_args()

    # utils.configure_colored_logging(args.loglevel)
    # utils.update_sanic_log_level()

    run(
        args.actions,
        args.port,
        args.cors,
        args.ssl_certificate,
        args.ssl_keyfile,
        args.ssl_password,
        args.auto_reload,
    )
