import os
import json
from utils.missing_audio_check import get_bucket_audio_files, get_template_audio_files, get_missing_audio_files
from logger_conf.logger import get_logger
from logger_conf.logger import get_logger, CustomAdapter


# name of the module for logging 
logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

with open("configs/parameters.json", "r", encoding="utf-8") as temp:
    params = json.load(temp)



logger.info("Stage: Verifying english Audios ******Jarvis english******")

template_path = params["english_template_path"]
logger.info("Template path : {} ".format(template_path))

bucket_path = params["bucket_path"]
logger.info("Bucket path for checking audios : {} ".format(bucket_path))

folder_name =  params["english_folder_name"]
logger.info("Folder anme for checking audios : {} ".format(folder_name))

# Pring the Entire S3 Path
print("s3://"+str(bucket_path)+"/"+str(folder_name))

logger.info("Complete path for checking audios : {} ".format(
    "s3://"+str(bucket_path)+"/"+str(folder_name)))

# Check the Audios
bucket_audio_files = get_bucket_audio_files(bucket_path, folder_name)
template_audio_files = get_template_audio_files(template_path)
missing_audio_files = get_missing_audio_files(template_audio_files, bucket_audio_files)

if not missing_audio_files:
    logger.info("All the english audios are present")
else:
    logger.info("Missing english Audios files in S3 are  :"+str(missing_audio_files))
    raise Exception("Missing english Audios files in S3")


