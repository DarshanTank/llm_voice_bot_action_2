FROM gnanivb.azurecr.io/rasa:1.10.16.v1.1
WORKDIR /nlp_server
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
EXPOSE 5002
ENTRYPOINT ["/bin/bash", "docker-entrypoint.sh"]
