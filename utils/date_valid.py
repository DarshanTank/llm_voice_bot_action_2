import sys
sys.path.append('../')
import datetime
import requests
import json
import yaml
import os
import re
from pytz import timezone
from db.tracker_store_connection import Haptik
from logger_conf.logger import get_logger
from dateutil import relativedelta
from db.tracker_store_connection import load_config, MongoDB, RedisDB
from raven import Client
from logger_conf.logger import get_logger, CustomAdapter


# name of the module for logging 
logger = get_logger(__name__) 
logger = CustomAdapter(logger, {"sender_id": None})

haptik_date = Haptik()


# loading endpoints
gen_config = load_config()
# redis connection
client = Client(gen_config["raven"]["url"])



format = "%d/%m/%Y %H:%M:%p"
# Current time in UTC
now_utc = datetime.datetime.now(timezone('UTC'))
# Convert to Asia/Kolkata time zone
now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
current_time_str = now_asia.strftime(format)
current_time= datetime.datetime.strptime(current_time_str, format).time()
current_date= datetime.datetime.strptime(current_time_str, format).date()

with open("../utils/regex.json", "r", encoding="utf-8") as temp:
    regex_dict = json.load(temp)


def validate_date(follow_date):

    date_format = "%d/%m/%Y"

    # followup date
    follow_up_date = datetime.datetime.strptime(follow_date, date_format).date()

    # current time
    today6pm = datetime.datetime.strptime('18:00:PM', "%H:%M:%p").time()
    today10am = datetime.datetime.strptime('10:00:AM', "%H:%M:%p").time()
        
    
    valid_date = (follow_up_date - current_date).days

    # today
    if valid_date == 0:
        #between 10 to 6
        if today10am < current_time < today6pm:
            return "valid"
        else:
            return "not_possible_today"
    # tomorrow and beyond
    elif valid_date >=1:
        return "valid"
    # before days
    else:
        return "invalid"

def evaluate_regex(lang, given_date):

    return_date = None
    date_format = "%d/%m/%Y"
    
    next_month_expression = regex_dict["next_month"][lang]
    next_month_regex_pattern = re.compile(next_month_expression)
    next_month_list = next_month_regex_pattern.findall(given_date)
    next_week_expression = regex_dict["next_week"][lang]
    next_week_regex_pattern = re.compile(next_week_expression)
    next_week_list = next_week_regex_pattern.findall(given_date)
    
    if len(next_month_list) > 0:
        return_date = datetime.date.today().replace(day=1) + relativedelta.relativedelta(months=1)
        logger.info("EVALUATE REGEX DATE IN THE NEXT MONTH IF COND:"+str(return_date))
    elif len(next_week_list) > 0:
        return_date = datetime.date.today() + relativedelta.relativedelta(days=7)

    if return_date is not None:
        return_date = return_date.strftime(date_format)
    # log return date
    logger.info("EVALUATE REGEX DATE:"+str(return_date))

    return return_date

def date_handling(lang, given_date):
    date_format = "%d/%m/%Y"
    if lang == 'hi':
        # some date handling
        if "परसों" in given_date or 'पर सो' in given_date:
            given_date=given_date.replace("पर सो","परसों")
        if 'मंडे' in given_date:
            given_date = given_date.replace("मंडे", "सोमवार")
        if 'ट्यूजडे' or 'ट्यूज डे' in given_date:
            given_date = given_date.replace("ट्यूजडे", "मंगलवार")
        if 'वेनसडे' in given_date or 'वेडनेसडे' in given_date or 'वेडनसडे' in given_date:
            given_date = given_date.replace("वेनसडे","बुधवार").replace('वेडनेसडे', 'बुधवार').replace('वेडनसडे', 'बुधवार')
        if 'थर्सडे' in given_date or 'गुरूवार' in given_date:
            given_date = given_date.replace("थर्सडे", "बृहस्पतिवार").replace('गुरूवार', 'बृहस्पतिवार')
        if 'फ्राइडे' in given_date or 'फ्राईडे' in given_date:
            given_date = given_date.replace("फ्राइडे", "शुक्रवार").replace('फ्राईडे', 'शुक्रवार')
        if 'सैटरडे' in given_date or 'सटरडे' in given_date:
            given_date = given_date.replace("सटरडे", "शनिवार").replace('सैटरडे', 'शनिवार')
        if 'संडे' in given_date:
            given_date = given_date.replace("संडे", "रविवार")
        if 'एप्रिल' in given_date or 'अप्रैल' in given_date:
            given_date = given_date.replace("एप्रिल", "अप्रैल").replace("अप्रैल","अप्रैल")
        if 'में' in given_date:
            given_date = given_date.replace("में", "मई")
        if 'फेबुरवरी' in given_date:
            given_date = given_date.replace('फेबुरवरी','feb')
        if 'एप्रिल' in given_date:
            given_date = given_date.replace('एप्रिल','apr')
        if 'जून' in given_date:
            given_date = given_date.replace('जून','june')
        if 'जुलाई' in given_date:
            given_date = given_date.replace('जुलाई','july')
        if 'सितम्बर' in given_date:
            given_date = given_date.replace('सितम्बर','sep')
        if 'सितंबर' in given_date:
            given_date = given_date.replace('सितंबर','sep')
        if 'अक्तूबर' in given_date:
            given_date = given_date.replace('अक्तूबर','oct')
        if 'नवम्बर' in given_date:
            given_date = given_date.replace('नवम्बर','nov')
        if 'नवंबर' in given_date:
            given_date = given_date.replace('नवंबर','nov')

        # handling for 'din baad' situations
        num = re.findall(r"[0-9]{1,3}", given_date)
        if 'बाद' in given_date and num:
            give_baad_date = datetime.datetime.today() + datetime.timedelta(days=int(num[0]))
            given_date = give_baad_date.strftime(date_format)

        if ('अगले' in given_date or 'नेक्स्ट' in given_date or 'अगला' in given_date) and ('वीक' in given_date or  'हफ़्ते' in given_date or 'हफ्ते' in given_date or 'सप्ताह' in given_date):
            num = 7
            date = datetime.datetime.today() + datetime.timedelta(days=int(num)) 
            given_date = give_baad_date.strftime(date_format)             
        
        # habdling week baad situations
        if 'बाद' in given_date and ('हफ्ते' in given_date or 'वीक' in given_date or 'हफ़्ते' in given_date or  'सप्ताह' in given_date) and num:
            week_no = int(num[0]) * 7
            date = datetime.datetime.today() + datetime.timedelta(days=int(week_no)) 
            given_date = give_baad_date.strftime(date_format)  
            
        return given_date
    else:
        return given_date

def haptik_date_validation(given_date, language):
    
    code = {
        # 'hindi' : 'hi',
        'english': 'en',
        

    }
    logger.info("DATE BEOFRE "+str(given_date))
    lang = code[language]
    given_date = date_handling(lang, given_date)
    logger.info("GIVEN DATE"+str(given_date))
    try:
        # date taking
        try:
            
            urldate = haptik_date.connect_haptik('date',given_date, lang)
            print("urlfor date:" + str(urldate))
            payload = {}
            headers = {}

            responseDate = requests.request("GET", urldate, headers=headers, data=payload)
            date = json.loads(responseDate.text.encode('utf8'))
        except Exception as e:
            client.captureException()
            logger.error("Error in request for Haptik date "+ str(e))

        if date['data'] is None:

            responseDate = evaluate_regex(lang,given_date)
            if responseDate is None:

                return 'not_proper_date'
            else:
                logger.info("Response after regex: "+ str(responseDate)) 
                return responseDate
            #return 'not_proper_date'
            
        else:
            dd = str(date['data'][0]['entity_value']['value']['dd'])
            mm = str(date['data'][0]['entity_value']['value']['mm'])
            yy = str(date['data'][0]['entity_value']['value']['yy'])

            follow_date = dd + '/' + mm + '/' + yy 
            logger.info("follow_date: "+ follow_date)       
    
            return follow_date

    except Exception as e:
        logger.error("Error in date taking "+ str(e))
        client.captureException()
