# -*- coding: utf-8 -*-
# Author: Roy
import sys
sys.path.append('../')
import datetime
import requests
import json
import re
from pytz import timezone
from db.tracker_store_connection import Haptik
from logger_conf.logger import get_logger, CustomAdapter
from db.tracker_store_connection import load_config

haptik_time = Haptik()

logger = get_logger(__name__)  # name of the module
logger = CustomAdapter(logger, {"sender_id": None})

format = "%d/%m/%Y %H:%M:%p"
# Current time in UTC
now_utc = datetime.datetime.now(timezone('UTC'))
# Convert to Asia/Kolkata time zone
now_asia = now_utc.astimezone(timezone('Asia/Kolkata'))
current_time_str = now_asia.strftime(format)
current_time= datetime.datetime.strptime(current_time_str, format).time()
current_date= datetime.datetime.strptime(current_time_str, format).date()

def haptik_time_validation(given_time):
    
    try:
        if 'साढ़े' in given_time:
            given_time = given_time.replace('साढ़े', 'साढे')
        if 'दस' in given_time:
            given_time = given_time.replace('दस','10')
        if 'डेढ़' in given_time:
            given_time = given_time.replace('डेढ़','1:30')
        if 'ढाई' in given_time:
            given_time = given_time.replace('ढाई','2:30')


        # handling for time without बजे
        a = re.findall(r"[0-9:]{1,5}", given_time)
        # if len(a) > 0:
        given_time = given_time.replace(a[0], a[0] + ' बजे ')


        # date taking
        try:
            urltime = haptik_time.connect_haptik('time',given_time)
            print("urlfor time:" + str(urltime))
            payload = {}
            headers = {}

            responseTime = requests.request("GET", urltime, headers=headers, data=payload)
            time = json.loads(responseTime.text.encode('utf8'))
        except Exception as e:
            logger.error("Error in request for Haptik time "+ str(e))

        if time['data'] is None:
            return 'not_proper_time'
            
        else:

            hour = str(time['data'][0]['entity_value']['hh'])
            min = str(time['data'][0]['entity_value']['mm'])
            am_or_pm = str(time['data'][0]['entity_value']['nn'])
            
            if  1 <= int(hour) <= 6 :
                am_or_pm = 'PM'
            if 10 <= int(hour) <= 11 and ("शाम" not in  given_time or 'रात' not in given_time):
                am_or_pm = 'AM'
             

            if am_or_pm == 'hrs' or am_or_pm == 'pm':
                am_or_pm = 'PM'
            if 'सुबह' in given_time:
                am_or_pm = 'AM'
            if "दिन" in given_time or 'दोपहर' in given_time or "शाम" in given_time or 'रात' in given_time:
                am_or_pm = 'PM'

            follow_time = hour + ':' + min + ':' + am_or_pm    
            logger.info("time obtained from haptik is: "+str(follow_time))


            return follow_time   
            
    except Exception as e:
        logger.error("Error in time taking "+ str(e))


def validate_only_time(time):
    

    # 10 am & 6 pm
    today10am = datetime.datetime.strptime('10:00:AM', "%H:%M:%p").time()
    print("today10am "+str(today10am))
    today6pm = datetime.datetime.strptime('18:00:PM', "%H:%M:%p").time()
    print("today6pm "+str(today6pm))
    print("time is = "+str(time))
    hour = int(time.split(':')[:-2][0])
    print("hour is = " + str(hour))
    
    # changing time to 24 hrs format
    if int(hour) <= 12:
        given_time = datetime.datetime.strptime(time,"%I:%M:%p").time()
        print("given_time "+str(given_time))
    elif 13 <= int(hour) <= 24:
        given_time = datetime.datetime.strptime(time,"%H:%M:%p").time()
    else:
        return 'utter_time_correct_format'

    if today10am <= given_time <= today6pm:

        return 'valid'        
    else:
        return 'utter_timeslot'


