#!/usr/bin/env python3
# -- coding: utf-8 --
# Author: Roy


import sys
sys.path.append('../')
from rasa.core.tracker_store import RedisTrackerStore 
from rasa_sdk import Tracker
from db.tracker_store_connection import load_config
from utils.text_format import color
from logger_conf.logger import get_logger , CustomAdapter
from raven import Client
from db.tracker_store_connection import load_config, MongoDB,RedisDB
import json
logger = get_logger(__name__)  # name of the module
logger = CustomAdapter(logger, {"sender_id": None})
# loading endpoints
gen_config = load_config()
# sentry connection
client = Client(gen_config["raven"]["url"])
# redis connection
redis_db_obj = RedisDB()

# bot responses templates json

with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
    templates_english = json.load(temp)

# with open("../configs/templates_english.json", "r", encoding="utf-8") as temp:
#     templates_english = json.load(temp)



# -----------------------------------------------------------------------------------------------------------------------
# colors and text format
red = color.RED
blue = color.BLUE
cyan = color.CYAN
darkcyan = color.DARKCYAN
green = color.GREEN
purple= color.PURPLE
yellow = color.YELLOW
end = color.END
# -----------------------------------------------------------------------------------------------------------------------
def get_template(language):
    lang_dict = {
        'english': templates_english,
    }

    if language in lang_dict:
        return lang_dict[language]


def template_resolver(language, key):
    lang_dict = {
        'english': templates_english
    }

    if language in lang_dict:
        return lang_dict[language][key]


def faq_body(tracker: Tracker):
    """
    Used for adding short messages at the end of each faqs
    return: short version of the main bot response
    """
    try:
        sender_id, cust_info = redis_db_obj.get_details(tracker)
        language = cust_info['language'].lower()
        logger.info("------------- ENTERED FAQ BODY-------------"+green+end, sender_id=sender_id)
        language_template = get_template(language)
        # product description
        full_name = str(cust_info['full_name'])
        language = str(cust_info['language']).lower()
        language=cust_info.get('language')
        variable_replacement_map = {
            "<full_name>":full_name,

        }
        # temporary templates for fallback messages
        # -------------------------------------------------------------------------------------
        templates_temp = {}
        # language_template = get_template(language)
        for key, value in language_template.items():
            for var_name, actual_variable in variable_replacement_map.items():
                value = value.replace(var_name,str(actual_variable))
            templates_temp[key] = value
        # --------------------------------------------------------------------------------------
    except Exception as e:
        logger.error("Exception!!! Failed to read data from redis database in FAQ" + str(e))
        client.captureException()
    msg_matched = False
    try:
        bot_msg = ""
        logger.info(f"----{bot_msg}------")
        for event in reversed(tracker.events):
            logger.info(f"event type is {event.get('event')}")
            if event.get("event") == "bot":
                for template_key,template_message in templates_temp.items():
                    action_msg = event.get("text")
                    # logger.info(f"action message is {action_msg}")
                    # logger.info(f"template message is {template_message}")
                    if action_msg == template_message:
                        logger.info("---------both are same---------")
                        if template_key.endswith("_short"):
                            logger.info("Key endswith short : "+str(template_key))
                            fallback_key = template_key.replace("_short","_short_2").replace("_fallback","_short") 
                        else:
                            logger.info("Key does not endswith short : "+str(template_key))
                            fallback_key = template_key + "_short"  # if there is fallback message is present
                        if fallback_key in templates_temp:
                            bot_msg = templates_temp[fallback_key]
                        else:
                            bot_msg = templates_temp[template_key]
                        msg_matched = True
                        break
                ## if short message is found end the loop   
                if msg_matched:
                    break
        logger.info("Exit FAQ function with response: "+green+str(bot_msg)+end, sender_id=sender_id)
        return bot_msg
    
    except Exception as e:
        logger.error(red+"Exception!!! Tracker events to get bot_msg in FAQ" + str(e)+end, sender_id=sender_id)
        client.captureException()
        bot_msg = ""
        return bot_msg