import time
import requests
import json

from sentence_transformers import SentenceTransformer
import torch
import pinecone

def model_initiatization():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    if device != 'cuda':
        print(f"You are using {device}. This is much slower than using a CUDA-enabled GPU. If on Colab you can change this by clicking Runtime > Change runtime type > GPU.")

    pinecone.init(
        api_key='1ff40176-644c-4204-a875-c79faf941e0b',
        environment='gcp-starter'
    )
    index = pinecone.Index('genai-voicebot')
    model = SentenceTransformer('all-MiniLM-L6-v2', device=device)
    return model, index

def get_unique_id():
    from datetime import datetime
    current_dateTime = datetime.now()
    current_dateTime= (current_dateTime.strftime("%Y:%m:%d:%H:%M:%S.%f")).replace(":","").replace(".","")
    return current_dateTime

def get_embedding(model,query):
    st = time.time()
    xq = model.encode(query).tolist()
    end = time.time()
    print(f"Time taken by get_embedding : {end-st}")
    return xq

def get_response_from_simmiler_input(model, pi_index,user_input_text):
    input_embed = get_embedding(model, user_input_text)
    response = pi_index.query(input_embed, top_k=1, include_metadata=True)
    print(f"\n\nPINECONE DB : \n\nUser input : {user_input_text} ----> \nSimiler response : {response}")
    
    if response != None and response and response['matches']!= [] and response['matches'][0]['score'] > 0.50:
        return True, response['matches'][0]['metadata']['response']
    else:
        return False, input_embed
        
def update_convo_history_prompt(current_Conversation:str,response_text:str,is_it_human_response:bool):
    
    if is_it_human_response:
        updated_response = f"""\n"Human": "{response_text}" """ 
    else:
        updated_response = f"""\n"Assistant": "{response_text}" """
    current_Conversation+=updated_response

    return current_Conversation

Conversation_history = """ 
"Assistant": "Hi, this is a call from Bajaj Finserv Limited. Are you interested in a personal loan?" 
"""
# example = {{"type":"completion","completion":"{"thinking": "Let me check the account details and payment history", "answer": \"Thanks for confirming Rohan. I'm calling about your outstanding loan balance. Do you have a few minutes to discuss your account and possible payment arrangements?" }","stop_reason":"stop_sequence","model":"claude-instant-1.2","stop":"\n\nHuman:","log_id":"a44da0251fbf071440dac8b211974b25c3d050f003b85891382b5d6e2c0bfc35"}}

def get_llm_response(redis_data, Conversation_history:str,user_input_text:str):
    start = time.time()
    print("="*30)
    
    url = "https://api.anthropic.com/v1/complete"
    Conversation_history = update_convo_history_prompt(Conversation_history, user_input_text,True)
    model, index = model_initiatization()
    
    # user_input_text
    is_response_present, response = get_response_from_simmiler_input(model, index,user_input_text)
    if is_response_present:
        # update convo and return response
        Conversation_history = update_convo_history_prompt(Conversation_history, response,False)
        end = time.time()
        print(f"time taken for already exisit response : {end-start} sec")
        return Conversation_history, response
    else:
        # get llm response + update and go ahead
        _id = get_unique_id()
        input_embed = response 
        
        # ===========================>  hit llm - start
        formate_of_response = {
            "thinking":"Ganesh is the CEO and founder of Gnani.ai",
            "answer":"Ganesh is the CEO and founder of Gnani.ai",
        }
        pompt_with_updated_convo_history = f"""
Human:You are a virtual assistant working for Bajaj Fin Serv limited. 
You are on call with the customer and inform them of the pre approved loan offer that they have and ask them if they are interested to avail it.
You are here to answer any queries on the loan, based on the details provided below.

Details:
1. Loan amount : 5 lakh rupees only.
2. Term : 56 months
3. EMI : 12000 rupees
4. Name of the person : Arvind

Also keep in mind the conversation flow:
1. For context you are speaking to Arvind, Who has showcase intrest in loan previously. You need to convinve him to take a loan.
2. You can tell the intention of the call only after the customer agrees to speak.
3. If user showcase negative response or less interest. Give him this response "Thank you, I will transfer the call to our customer service executive EOC"
4. When you are giving conversation ending response, Do add "EOC" after your response. EOC is used as trigger for ending a call.
5. Make sure that your response don't have any special characters.
You will enter this role after the instruction Begin Dialogue 

Remember response should be really short and maximum 200.
 
Remember dont greet customer every time with Hi,Hello...etc. This is not required, 
Put your answer to the user inside answer JSON key Remember response should be a JSON {formate_of_response}.
Answer based on the question asked, keeping in mind the conversation history. 
BEGIN DIALOGUE
 
{Conversation_history}
Assistant:
"""

        #FAQ="Founders: Ananth - CTO and Ganesh CEO; Sales_Team : Sanjay Pai - CRO; Engineering: Bharath Shankar - VP Engineering; Products:Automate 365 - Omnichannel bots, Aura 365 - Post-facto analytics, Assist 365 - Real time agent assist, Armour 365 - Voicebiometrics.ai; HQ: Bangalore, India; Funding:Samsung Ventures; Thank you: Thanks; Partners:Concentrix,TCS,LTI Mindtree,CS Infocom..etc"
    #     Question="Yeah this is Rohan"
        payload = json.dumps({
            "model": "claude-instant-1.2",
            "max_tokens_to_sample": 256,
            "stream": False,
            "prompt": pompt_with_updated_convo_history  #"Human: You are now tasked with functioning as a virtual agent specializing in debt collection through phone calls. Your objective is to engage with individuals who owe money in a professional and empathetic manner. Emphasize the importance of obtaining payment commitments while maintaining a respectful demeanor throughout the conversation. Please prioritize building rapport, addressing potential concerns, and offering flexible solutions to encourage debtors to fulfill their obligations. Be sure to adhere strictly to relevant legal and ethical guidelines in the debt collection process. Your ultimate goal is to negotiate successful payment arrangements and contribute to the overall efficiency and effectiveness of the debt recovery process. Remember while negotiating the lowest minimum payable amount that you can offer will be 5000 and by increasing months to 48 from the current 11 months. You will enter this role after the instruction Begin Dialogue For context you are speaking to Rohan, who owes rupees 20000 and his due date has lapsed three days ago. Also, for conversation history is :"+conv_history+".Remember response should be really short and maximum 200. Put your answer to the user inside answer JSON key Remember response should be a JSON {\r\n\"thinking\":\"Ganesh is the CEO and founder of Gnani.ai\",\r\n\"answer\":\"Ganesh is the CEO and founder of Gnani.ai\"\r\n}. Remember dont greet customer every time with Hi,Hello...etc. This is not required, answer based on the question asked, keeping in mind the conversation history. BEGIN DIALOGUE\n\n "+conv_history+" \"Human\":"+Question+"\n\nAssistant:"
            })
            

        headers = {
            'accept': 'application/json',
            'anthropic-version': '2023-06-01',
            'content-type': 'application/json',
            'x-api-key': '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'
            }
            
        response = requests.request("POST", url, headers=headers, data=payload)
        resp = response.text
            
        resp = json.dumps(resp)
        response_dict=json.loads(json.loads(resp))
        print(response_dict)
        response_string = response_dict["completion"]
        print(response_string)
        print("response_string_json (before): ",response_string," \n response_string_json type is :",type(response_string))
        response_string = json.loads(response_string.replace("'",'"').replace('"m',"'m").replace('"re',"'re").replace('"d',"'d").replace('"ll',"'ll").replace('"ve',"'ve").replace('"s',"'s"))
        print("response_string_json (after): ",response_string," \n response_string_json type is :",type(response_string))
        response_string = response_string["answer"]
        
        # ===========================>  hit llm - end
        
        try:
            vec = [{
                    'id':_id,
                    'values':input_embed,
                    'metadata':{"question":user_input_text,"response":response_string}
                }]
                
            index.upsert(vectors=vec)
        except:
            pass
        Conversation_history = update_convo_history_prompt(Conversation_history, response_string,False)
        
        print("\nCONVO:\n",Conversation_history)
        end = time.time()
        print(f"time taken for llm response + VDB update : {end-start} sec")
        print("="*30)
        return Conversation_history,response_string
